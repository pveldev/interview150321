﻿using interview.DAL.Models;
using interview.DAL.QueryObjects.Realizations;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Interview.XmlClient.Implementation
{
    public class Rates  : List<ExchangeRate>{ }

    public class XmlClient
    {
        const string _exchangeRatesPath = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=";
        const string _currenciesPath = "http://www.cbr.ru/scripts/XML_valFull.asp";
        private readonly CurrencyQo _currencyQo;
        private readonly CommonQo _commonQo;
        private readonly ExchangeRateQo _exchangeRateQo;

        public XmlClient(CurrencyQo currencyQo, CommonQo commonQo, ExchangeRateQo exchangeRateQo)
        {
            _currencyQo = currencyQo;
            _commonQo = commonQo;
            _exchangeRateQo = exchangeRateQo;
        }

        public async Task<List<ExchangeRate>> GetExchangeRateXmlAsync(IEnumerable<Currency> addedCurrencies, DateTime date)
        {
            try
            {
                date = date.Date;
                string apiUrl = string.Concat(_exchangeRatesPath, date.ToString("dd/MM/yyyy"));

                Dictionary<string, Currency> currencies = addedCurrencies
                    //.Where(currency => currency.IsoNumCode != -1)
                    .ToDictionary(currency => currency.CbrKey);
            
                if (!currencies?.Any() ?? true)
                {
                    Console.WriteLine("We haven't currencies in our db");
                    return null;
                }
           
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                XDocument doc = XDocument.Load(apiUrl);

                IEnumerable<ExchangeRate> parsed = doc.Elements("ValCurs").Elements("Valute").Select(xElement =>
                {
                    return new ExchangeRate()
                    {
                        CurrencyId = TryGetCurrencyId(currencies, xElement.Attribute("ID")?.Value),
                        ExchangeDate = date,
                        Value = ParseToDouble(xElement.Element("Value")?.Value),
                        UpdateDate = DateTime.Now
                    };
                });

                if (await _commonQo.UpsertEntitysAsync(parsed))
                {
                    return await _exchangeRateQo.GetByDateOrEmpty(date);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return new List<ExchangeRate>();
            }
        }

        public async Task<List<Currency>> GetCurrencyXmlAsync()
        {
            try
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                XDocument doc = XDocument.Load(_currenciesPath);

                Dictionary<string, Currency> addedCurrencies = await _currencyQo.GetAllOrEmptyDictionary();

                IEnumerable<Currency> parsed = doc.Elements("Valuta").Elements("Item").Select(xElement =>
                {
                    string cbrKey = ParseCbrKey(xElement);

                    if (addedCurrencies.TryGetValue(cbrKey, out Currency addedCurrency))
                    {
                        return UpdateCurrencyFields(addedCurrency, xElement, cbrKey);
                    }
                    else
                    {
                        return CreateNewCurrency(xElement, cbrKey);
                    }             
                });

                if (await _commonQo.UpsertEntitysAsync(parsed))
                {
                    return await _currencyQo.GetAllOrEmpty();
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }          
        }

        private Currency CreateNewCurrency(XElement xElement, string cbrKey) => new Currency()
        {
            CbrKey = cbrKey,
            IsoNumCode = ParseIsoNumCode(xElement),
            Name = ParseName(xElement),
            EngName = ParseEngName(xElement),
            Nominal = ParseNominal(xElement),
            IsoCharCode = ParseIsoCharCode(xElement),
            UpdateDate = DateTime.Now
        };     

        private Currency UpdateCurrencyFields(Currency currency, XElement xElement, string cbrKey)
        {
            currency.IsoNumCode = ParseIsoNumCode(xElement);
            currency.CbrKey = cbrKey;
            currency.Name = ParseName(xElement);
            currency.EngName = ParseEngName(xElement);
            currency.Nominal = ParseNominal(xElement);
            currency.IsoCharCode = ParseIsoCharCode(xElement);
            currency.UpdateDate = DateTime.Now;

            return currency;
        }

        private string ParseCbrKey(XElement xElement) => xElement.Attribute("ID")?.Value ?? CreateUniqueKey(xElement);
        private string ParseName(XElement xElement) => xElement.Element("Name")?.Value;
        private string ParseEngName(XElement xElement) => xElement.Element("EngName")?.Value;
        private int ParseNominal(XElement xElement) => ParseToInt(xElement.Element("Nominal")?.Value);
        private string ParseIsoCharCode(XElement xElement) => xElement.Element("ISO_Char_Code")?.Value ?? "empty";
        private int ParseIsoNumCode(XElement xElement) => ParseToInt(xElement.Element("ISO_Num_Code")?.Value);

        private string CreateUniqueKey(XElement xElement) => DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff", CultureInfo.InvariantCulture) + xElement.Element("Name")?.Value;

        private int ParseToInt(string input) => int.TryParse(input, out int parsed) ? parsed : -1;
        private decimal ParseToDecimal(string input) => decimal.TryParse(input, out decimal exchangeValue) ? exchangeValue : -1;
        private double ParseToDouble(string input) => double.TryParse(input, out double exchangeValue) ? exchangeValue : -1;
        private int TryGetCurrencyId<T>(Dictionary<T, Currency> currencies, T key) => currencies.TryGetValue(key, out Currency currency) ? currency.Id : 0;
    } 
}
