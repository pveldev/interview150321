﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace interview.DAL.Models.Common
{
    public class BasicModel
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime UpdateDate { get; set; }
    }
}
