﻿using interview.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Xml.Serialization;

namespace interview.DAL.Models
{
    [Serializable]
    public partial class ExchangeRate
    {
        public int CurrencyId { get; set; }

        public double Value { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime ExchangeDate { get; set; }

        [XmlIgnore]
        public virtual Currency Currency { get; set; }
    }
}
