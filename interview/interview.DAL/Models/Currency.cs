﻿using interview.DAL.Models.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace interview.DAL.Models
{
    [Serializable]
    public partial class Currency
    {
        public Currency()
        {
            ExchangeRates = new HashSet<ExchangeRate>();
        }

        [Required]
        [StringLength(10)]
        public string CbrKey { get; set; }
      
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string EngName { get; set; }

        [Required]
        public int IsoNumCode { get; set; }

        [Required]
        [StringLength(3)]
        public string IsoCharCode { get; set; }

        public int Nominal { get; set; }

        public virtual ICollection<ExchangeRate> ExchangeRates { get; set; }
    }
}
