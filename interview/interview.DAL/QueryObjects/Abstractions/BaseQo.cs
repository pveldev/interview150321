﻿using interview.DAL.Core;
using interview.DAL.Models.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace interview.DAL.QueryObjects.Abstractions
{
    public class BaseQO
    {
        protected readonly InterviewContext _context;

        public BaseQO(InterviewContext context)
        {
            _context = context;
        }     
    }
}
