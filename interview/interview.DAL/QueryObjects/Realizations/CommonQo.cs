﻿using interview.DAL.Core;
using interview.DAL.Models.Common;
using interview.DAL.QueryObjects.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace interview.DAL.QueryObjects.Realizations
{
    public class CommonQo : BaseQO
    {
        public CommonQo(InterviewContext context) : base(context) { }

        public async Task<bool> UpsertEntityAsync<T>(T entity) where T : BasicModel
        {
            try
            {
                _context.Entry(entity).State = entity.Id == 0 ?
                    EntityState.Added
                    :
                    EntityState.Modified;

                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpsertEntitysAsync<T>(IEnumerable<T> entities) where T : BasicModel
        {
            try
            {
                foreach (var entity in entities)
                {
                    _context.Entry(entity).State = entity.Id == 0 ?
                        EntityState.Added
                        :
                        EntityState.Modified;
                }

                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)            
            {
                return false;
            }
        }
    }
}
