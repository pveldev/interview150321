﻿using interview.DAL.Core;
using interview.DAL.Models;
using interview.DAL.QueryObjects.Abstractions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interview.DAL.QueryObjects.Realizations
{
    public class ExchangeRateQo : BaseQO
    {
        public ExchangeRateQo(InterviewContext context) : base(context) { }

        public async Task<List<ExchangeRate>> GetAllOrEmpty()
        {
            try
            {
                return await _context.ExchangeRates.ToListAsync();
            }
            catch (Exception ex)
            {
                return new List<ExchangeRate>();
            }
        }

        public async Task<List<ExchangeRate>> GetByDateOrEmpty(DateTime date)
        {
            try
            {
                return await _context.ExchangeRates.Where(exchangeRate => exchangeRate.ExchangeDate == date).ToListAsync();
            }
            catch (Exception ex)
            {
                return new List<ExchangeRate>();
            }
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(int isoNumCode, DateTime date)
        {
            try
            {
                return await _context.ExchangeRates
                    .OrderByDescending(exchangeRate => exchangeRate.UpdateDate)
                    .FirstOrDefaultAsync(exchangeRate => exchangeRate.ExchangeDate == date && exchangeRate.Currency.IsoNumCode == isoNumCode);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ExchangeRate> GetExchangeRateWithSqlAsync(int isoNumCode, DateTime date)
        {
            try
            {
                string query = @"
SELECT TOP 1 er.* FROM ExchangeRates er 
INNER JOIN Currencies c ON c.Id = er.CurrencyId
WHERE er.ExchangeDate >= @date AND  c.IsoNumCode = @isoNumCode
ORDER BY er.UpdateDate DESC
                                ";

                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@isoNumCode", isoNumCode),
                    new SqlParameter("@date", date),
                };

                return await _context.ExchangeRates.FromSqlRaw(query, sqlParams).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                return null;
            }
        }    
    }
}
