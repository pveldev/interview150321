﻿using interview.DAL.Core;
using interview.DAL.Models;
using interview.DAL.QueryObjects.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interview.DAL.QueryObjects.Realizations
{
    public class CurrencyQo : BaseQO
    {
        public CurrencyQo(InterviewContext context) : base(context) {}

        public async Task<List<Currency>> GetAllOrEmpty()
        {
            try
            {
                return await _context.Currencies.ToListAsync();
            }
            catch (Exception ex)
            {
                return new List<Currency>();
            }
        }

        public async Task<Dictionary<string, Currency>> GetAllOrEmptyDictionary()
        {
            try
            {
                return (await _context.Currencies.ToListAsync()).ToDictionary(currency => currency.CbrKey);
            }
            catch (Exception ex)
            {
                return new Dictionary<string, Currency>();
            }
        }
    }
}
