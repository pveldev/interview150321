﻿using interview.DAL.Core;
using interview.DAL.Models;
using interview.DAL.QueryObjects.Realizations;
using Interview.ConsoleDebugger.ConsoleCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;

namespace Interview.ConsoleDebugger
{
    class Program
    {
        static ConsoleClient client;
        static bool _appIsRunning = false;
        static bool _refreshIsRunning = false;

        static void Main(string[] args)
        {
            Console.WriteLine(GetCommands());

            _appIsRunning = true;
            client = new ConsoleClient();

            Timer timer = new Timer { Interval = 3000 };
            timer.Elapsed += OnTimerElapsedEvent;
            timer.AutoReset = true;
            timer.Start();

            while (_appIsRunning)
            {
                string command = Console.ReadLine();
                Console.WriteLine(ExecuteCommand(command));
            }
        }

        private async static void OnTimerElapsedEvent(object sender, ElapsedEventArgs e)
        {
            if (_refreshIsRunning)
            {
                Console.WriteLine($"Refresh method is starting to work.\t[{DateTime.Now}]");
                await client.DoWorkAsync();
                Console.WriteLine($"Refresh method finished the work.\t[{DateTime.Now}]");
            }        
        }
        private static string ExecuteCommand(string command)
        {
            return command switch
            {
                "quit" => StopAppWorkCommand(),
                "start" => StartRefreshingCommand(),
                "stop" => StopRefreshingCommand(),
                "1" => ExecuteCommand(withSql: true),
                "2" => ExecuteCommand(withSql: false),
                _ => UnknownCommandProcessing()
            };
        }

        private static string GetCommands() => "quit, start, stop, 1 , 2\n1 is sql, 2 is ef";
        private static string UnknownCommandProcessing() => string.Concat("unknown command. Use: ", GetCommands());    

        private static string StartRefreshingCommand()
        {
            _appIsRunning = true;
            return "started";
        }

        private static string StopAppWorkCommand()
        {
            _appIsRunning = false;
            return "stopped";
        }

        private static string StopRefreshingCommand()
        {
            _refreshIsRunning = false;
            return "refreshing stopped";
        }      

        private static string ExecuteCommand(bool withSql)
        {
            _appIsRunning = false;

            Console.Write("Enter isoNumCode ");
            string stringIsoNumCode = Console.ReadLine();

            Console.Write("Enter date (format: dd/mm/yy) ");
            string stringDate = Console.ReadLine();

            if (int.TryParse(stringIsoNumCode, out int isoNumCode) && DateTime.TryParse(stringDate, out DateTime date))
            {
                ExchangeRate exchangeRate = withSql ?
                    client.GetWithSqlAsync(isoNumCode, date).Result
                    :
                    client.GetWithEfAsync(isoNumCode, date).Result;

                if (exchangeRate is null)
                {
                    _appIsRunning = true;
                    return "Empty result!";
                }

                string additionalInfo = date.Date != exchangeRate.ExchangeDate.Date ?
                    $"[api reterned {exchangeRate.ExchangeDate.Date} with query {date.Date}]"
                    : "";

                _appIsRunning = true;
                return string.Concat("exchange rate: ", exchangeRate?.Value.ToString(), " exchange date: ", exchangeRate?.ExchangeDate, additionalInfo);
            }
            else
            {
                _appIsRunning = true;
                return "Command error (wrong user input)";
            }
        }
    }
}
