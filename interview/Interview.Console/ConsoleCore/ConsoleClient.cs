﻿using interview.DAL.Core;
using interview.DAL.Models;
using interview.DAL.QueryObjects.Realizations;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interview.ConsoleDebugger.ConsoleCore
{
    public class ConsoleClient
    {
        const string _baseConnectionString = @"Server=.\SQLEXPRESS;Database=InterviewDb;Trusted_Connection=True;";
        static readonly DateTime _exchangeDate = DateTime.Now;
        static XmlClient.Implementation.XmlClient client;
        static CurrencyQo _currencyQo;
        static CommonQo _commonQo;
        static ExchangeRateQo _exchangeRateQo;

        public ConsoleClient()
        {
            Init();
        }

        private void Init()
        {
            InterviewContext context = new InterviewContext(_baseConnectionString);

            _currencyQo = new CurrencyQo(context);
            _commonQo = new CommonQo(context);
            _exchangeRateQo = new ExchangeRateQo(context);

            client = new XmlClient.Implementation.XmlClient(_currencyQo, _commonQo, _exchangeRateQo);
        }

        public async Task DoWorkAsync()
        {
            List<Currency> refreshedCurrencies = await client.GetCurrencyXmlAsync();

            List<ExchangeRate> exchangeRates = await client.GetExchangeRateXmlAsync(refreshedCurrencies, _exchangeDate);
        }

        public async Task<ExchangeRate> GetWithEfAsync(int isoNumCode, DateTime date) => await _exchangeRateQo.GetExchangeRateAsync(isoNumCode, date);
        public async Task<ExchangeRate> GetWithSqlAsync(int isoNumCode, DateTime date) => await _exchangeRateQo.GetExchangeRateWithSqlAsync(isoNumCode, date);     
    }
}
