USE [master]
GO
/****** Object:  Database [InterviewDb]    Script Date: 17.03.2021 0:16:02 ******/
CREATE DATABASE [InterviewDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'InterviewDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\InterviewDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'InterviewDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\InterviewDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [InterviewDb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [InterviewDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [InterviewDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [InterviewDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [InterviewDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [InterviewDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [InterviewDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [InterviewDb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [InterviewDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [InterviewDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [InterviewDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [InterviewDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [InterviewDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [InterviewDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [InterviewDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [InterviewDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [InterviewDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [InterviewDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [InterviewDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [InterviewDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [InterviewDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [InterviewDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [InterviewDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [InterviewDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [InterviewDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [InterviewDb] SET  MULTI_USER 
GO
ALTER DATABASE [InterviewDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [InterviewDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [InterviewDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [InterviewDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [InterviewDb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [InterviewDb] SET QUERY_STORE = OFF
GO
USE [InterviewDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 17.03.2021 0:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 17.03.2021 0:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CbrKey] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[EngName] [nvarchar](100) NULL,
	[IsoNumCode] [int] NOT NULL,
	[IsoCharCode] [nvarchar](3) NOT NULL,
	[Nominal] [int] NOT NULL,
	[UpdateDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExchangeRates]    Script Date: 17.03.2021 0:16:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangeRates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyId] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[ExchangeDate] [datetime2](7) NOT NULL,
	[UpdateDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ExchangeRates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210315162319_Initial', N'5.0.4')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210316180400_UpdateExRateType', N'5.0.4')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210316194319_AddedUpdateDate', N'5.0.4')
SET IDENTITY_INSERT [dbo].[Currencies] ON 

INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (739, N'R01010', N'Австралийский доллар', N'Australian Dollar', 36, N'AUD', 1, CAST(N'2021-03-16T23:22:50.4856246' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (740, N'R01510A', N'Немецкая марка', N'Deutsche Mark', 280, N'DEM', 100, CAST(N'2021-03-16T23:22:50.5020735' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (741, N'R01523', N'Нидерландский гульден', N'Netherlands Gulden', 528, N'NLG', 100, CAST(N'2021-03-16T23:22:50.5020967' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (742, N'R01535', N'Норвежская крона', N'Norwegian Krone', 578, N'NOK', 10, CAST(N'2021-03-16T23:22:50.5021178' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (743, N'R01565', N'Польский злотый', N'Polish Zloty', 985, N'PLN', 1, CAST(N'2021-03-16T23:22:50.5021389' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (744, N'R01570', N'Португальский эскудо', N'Portuguese Escudo', 620, N'PTE', 10000, CAST(N'2021-03-16T23:22:50.5021598' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (745, N'R01585', N'Румынский лей', N'Romanian Leu', 642, N'ROL', 10000, CAST(N'2021-03-16T23:22:50.5021853' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (746, N'R01585F', N'Румынский лей', N'Romanian Leu', 946, N'RON', 10, CAST(N'2021-03-16T23:22:50.5022068' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (747, N'R01589', N'СДР (специальные права заимствования)', N'SDR', 960, N'XDR', 1, CAST(N'2021-03-16T23:22:50.5022306' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (748, N'R01625', N'Сингапурский доллар', N'Singapore Dollar', 702, N'SGD', 1, CAST(N'2021-03-16T23:22:50.5022517' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (749, N'R01665A', N'Суринамский доллар', N'Surinam Dollar', 968, N'SRD', 1, CAST(N'2021-03-16T23:22:50.5022726' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (750, N'R01670', N'Таджикский сомони', N'Tajikistan Ruble', 972, N'TJS', 10, CAST(N'2021-03-16T23:22:50.5022934' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (751, N'R01670B', N'Таджикский рубл', N'Tajikistan Ruble', 762, N'TJR', 10, CAST(N'2021-03-16T23:22:50.5023166' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (752, N'R01700J', N'Турецкая лира', N'Turkish Lira', 949, N'TRY', 1, CAST(N'2021-03-16T23:22:50.5023378' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (753, N'R01710', N'Туркменский манат', N'Turkmenistan Manat', 795, N'TMM', 10000, CAST(N'2021-03-16T23:22:50.5023587' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (754, N'R01710A', N'Новый туркменский манат', N'New Turkmenistan Manat', 934, N'TMT', 1, CAST(N'2021-03-16T23:22:50.5023795' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (755, N'R01717', N'Узбекский сум', N'Uzbekistan Sum', 860, N'UZS', 1000, CAST(N'2021-03-16T23:22:50.5024024' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (756, N'R01720', N'Украинская гривна', N'Ukrainian Hryvnia', 980, N'UAH', 10, CAST(N'2021-03-16T23:22:50.5024232' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (757, N'R01720A', N'Украинский карбованец', N'Ukrainian Hryvnia', -1, N'', 1, CAST(N'2021-03-16T23:22:50.5024440' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (758, N'R01740', N'Финляндская марка', N'Finnish Marka', 246, N'FIM', 100, CAST(N'2021-03-16T23:22:50.5024648' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (759, N'R01750', N'Французский франк', N'French Franc', 250, N'FRF', 1000, CAST(N'2021-03-16T23:22:50.5024886' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (760, N'R01760', N'Чешская крона', N'Czech Koruna', 203, N'CZK', 10, CAST(N'2021-03-16T23:22:50.5025095' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (761, N'R01770', N'Шведская крона', N'Swedish Krona', 752, N'SEK', 10, CAST(N'2021-03-16T23:22:50.5025306' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (762, N'R01775', N'Швейцарский франк', N'Swiss Franc', 756, N'CHF', 1, CAST(N'2021-03-16T23:22:50.5025513' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (763, N'R01790', N'ЭКЮ', N'ECU', 954, N'XEU', 1, CAST(N'2021-03-16T23:22:50.5025789' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (764, N'R01795', N'Эстонская крона', N'Estonian Kroon', 233, N'EEK', 10, CAST(N'2021-03-16T23:22:50.5026004' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (765, N'R01805', N'Югославский новый динар', N'Yugoslavian Dinar', 890, N'YUN', 1, CAST(N'2021-03-16T23:22:50.5026212' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (766, N'R01810', N'Южноафриканский рэнд', N'S.African Rand', 710, N'ZAR', 10, CAST(N'2021-03-16T23:22:50.5026420' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (767, N'R01510', N'Немецкая марка', N'Deutsche Mark', 276, N'DEM', 1, CAST(N'2021-03-16T23:22:50.5020525' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (768, N'R01815', N'Вон Республики Корея', N'South Korean Won', 410, N'KRW', 1000, CAST(N'2021-03-16T23:22:50.5026659' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (769, N'R01500', N'Молдавский лей', N'Moldova Lei', 498, N'MDL', 10, CAST(N'2021-03-16T23:22:50.5020317' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (770, N'R01435', N'Литовский лит', N'Lithuanian Lita', 440, N'LTL', 1, CAST(N'2021-03-16T23:22:50.5019862' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (771, N'R01015', N'Австрийский шиллинг', N'Austrian Shilling', 40, N'ATS', 1000, CAST(N'2021-03-16T23:22:50.5013325' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (772, N'R01020A', N'Азербайджанский манат', N'Azerbaijan Manat', 944, N'AZN', 1, CAST(N'2021-03-16T23:22:50.5013936' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (773, N'R01035', N'Фунт стерлингов Соединенного королевства', N'British Pound Sterling', 826, N'GBP', 1, CAST(N'2021-03-16T23:22:50.5014168' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (774, N'R01040F', N'Ангольская новая кванза', N'Angolan new Kwanza', 24, N'AON', 100000, CAST(N'2021-03-16T23:22:50.5014391' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (775, N'R01060', N'Армянский драм', N'Armenia Dram', 51, N'AMD', 1000, CAST(N'2021-03-16T23:22:50.5014704' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (776, N'R01090B', N'Белорусский рубль', N'Belarussian Ruble', 933, N'BYN', 1, CAST(N'2021-03-16T23:22:50.5014931' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (777, N'R01095', N'Бельгийский франк', N'Belgium Franc', 56, N'BEF', 1000, CAST(N'2021-03-16T23:22:50.5015141' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (778, N'R01100', N'Болгарский лев', N'Bulgarian lev', 975, N'BGN', 1, CAST(N'2021-03-16T23:22:50.5015357' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (779, N'R01115', N'Бразильский реал', N'Brazil Real', 986, N'BRL', 1, CAST(N'2021-03-16T23:22:50.5015601' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (780, N'R01135', N'Венгерский форинт', N'Hungarian Forint', 348, N'HUF', 100, CAST(N'2021-03-16T23:22:50.5015814' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (781, N'R01200', N'Гонконгский доллар', N'Hong Kong Dollar', 344, N'HKD', 10, CAST(N'2021-03-16T23:22:50.5016025' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (782, N'R01205', N'Греческая драхма', N'Greek Drachma', 300, N'GRD', 10000, CAST(N'2021-03-16T23:22:50.5016234' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (783, N'R01215', N'Датская крона', N'Danish Krone', 208, N'DKK', 10, CAST(N'2021-03-16T23:22:50.5016479' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (784, N'R01235', N'Доллар США', N'US Dollar', 840, N'USD', 1, CAST(N'2021-03-16T23:22:50.5016704' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (785, N'R01239', N'Евро', N'Euro', 978, N'EUR', 1, CAST(N'2021-03-16T23:22:50.5016914' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (786, N'R01270', N'Индийская рупия', N'Indian Rupee', 356, N'INR', 100, CAST(N'2021-03-16T23:22:50.5017125' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (787, N'R01305', N'Ирландский фунт', N'Irish Pound', 372, N'IEP', 100, CAST(N'2021-03-16T23:22:50.5017407' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (788, N'R01310', N'Исландская крона', N'Iceland Krona', 352, N'ISK', 10000, CAST(N'2021-03-16T23:22:50.5017651' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (789, N'R01315', N'Испанская песета', N'Spanish Peseta', 724, N'ESP', 10000, CAST(N'2021-03-16T23:22:50.5017862' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (790, N'R01325', N'Итальянская лира', N'Italian Lira', 380, N'ITL', 100000, CAST(N'2021-03-16T23:22:50.5018073' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (791, N'R01335', N'Казахстанский тенге', N'Kazakhstan Tenge', 398, N'KZT', 100, CAST(N'2021-03-16T23:22:50.5018313' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (792, N'R01350', N'Канадский доллар', N'Canadian Dollar', 124, N'CAD', 1, CAST(N'2021-03-16T23:22:50.5018524' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (793, N'R01370', N'Киргизский сом', N'Kyrgyzstan Som', 417, N'KGS', 100, CAST(N'2021-03-16T23:22:50.5018733' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (794, N'R01375', N'Китайский юань', N'China Yuan', 156, N'CNY', 10, CAST(N'2021-03-16T23:22:50.5018940' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (795, N'R01390', N'Кувейтский динар', N'Kuwaiti Dinar', 414, N'KWD', 10, CAST(N'2021-03-16T23:22:50.5019210' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (796, N'R01405', N'Латвийский лат', N'Latvian Lat', 428, N'LVL', 1, CAST(N'2021-03-16T23:22:50.5019444' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (797, N'R01420', N'Ливанский фунт', N'Lebanese Pound', 422, N'LBP', 100000, CAST(N'2021-03-16T23:22:50.5019650' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (798, N'R01436', N'Литовский талон', N'Lithuanian talon', -1, N'', 1, CAST(N'2021-03-16T23:22:50.5020105' AS DateTime2))
INSERT [dbo].[Currencies] ([Id], [CbrKey], [Name], [EngName], [IsoNumCode], [IsoCharCode], [Nominal], [UpdateDate]) VALUES (799, N'R01820', N'Японская иена', N'Japanese Yen', 392, N'JPY', 100, CAST(N'2021-03-16T23:22:50.5026871' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Currencies] OFF
SET IDENTITY_INSERT [dbo].[ExchangeRates] ON 

INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2486, 799, 67.105, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3420990' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2487, 773, 102.0337, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3413492' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2488, 775, 13.8828, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3413710' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2489, 776, 28.2399, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3414025' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2490, 778, 44.6725, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3414263' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2491, 779, 13.1899, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3414513' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2492, 780, 23.8136, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3414764' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2493, 781, 94.3136, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3415068' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2494, 783, 11.7498, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3415346' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2495, 784, 73.2317, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3415617' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2496, 785, 87.3508, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3415932' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2497, 786, 10.1029, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3416163' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2498, 791, 17.4756, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3416341' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2499, 792, 58.7499, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3416549' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2500, 793, 86.3305, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3416730' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2501, 794, 11.2612, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3416901' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2502, 769, 41.577, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3417105' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2503, 742, 86.7356, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3417287' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2504, 768, 64.5298, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3420677' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2505, 766, 49.0855, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3420349' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2506, 762, 78.8031, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3419957' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2507, 761, 86.0466, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3419629' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2508, 760, 33.4041, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3419338' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2509, 756, 26.4088, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418990' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2510, 772, 43.1028, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3411340' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2511, 755, 69.6191, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418816' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2512, 752, 96.8596, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418438' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2513, 750, 64.2665, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418263' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2514, 748, 54.4231, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418092' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2515, 747, 104.591, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3417914' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2516, 746, 17.8858, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3417637' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2517, 743, 19.0673, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3417462' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2518, 754, 20.9533, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3418644' AS DateTime2))
INSERT [dbo].[ExchangeRates] ([Id], [CurrencyId], [Value], [ExchangeDate], [UpdateDate]) VALUES (2519, 739, 56.7033, CAST(N'2021-03-16T00:00:00.0000000' AS DateTime2), CAST(N'2021-03-16T23:21:30.3208330' AS DateTime2))
SET IDENTITY_INSERT [dbo].[ExchangeRates] OFF
/****** Object:  Index [IX_ExchangeRates_CurrencyId]    Script Date: 17.03.2021 0:16:02 ******/
CREATE NONCLUSTERED INDEX [IX_ExchangeRates_CurrencyId] ON [dbo].[ExchangeRates]
(
	[CurrencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Currencies] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [UpdateDate]
GO
ALTER TABLE [dbo].[ExchangeRates] ADD  DEFAULT ('0001-01-01T00:00:00.0000000') FOR [UpdateDate]
GO
ALTER TABLE [dbo].[ExchangeRates]  WITH CHECK ADD  CONSTRAINT [FK_ExchangeRates_Currencies_CurrencyId] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExchangeRates] CHECK CONSTRAINT [FK_ExchangeRates_Currencies_CurrencyId]
GO
USE [master]
GO
ALTER DATABASE [InterviewDb] SET  READ_WRITE 
GO
