Use InterviewDb

SELECT TOP 1 er.* FROM ExchangeRates er 
INNER JOIN Currencies c ON c.Id = er.CurrencyId
WHERE er.ExchangeDate >= @date AND  c.IsoNumCode = @isoNumCode
ORDER BY er.UpdateDate DESC